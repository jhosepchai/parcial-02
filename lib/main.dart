import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Material App',
      home: Scaffold(
          appBar: AppBar(
            leading: Icon(Icons.account_box_rounded),
            backgroundColor: Colors.red,
            title: Text('Employee List'),
          ),
          body: Column(
            children: [
              Text("Josue Emanuel Lopez Zamora 25-2880-2017",
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15)),
              Text("Jhosep Isaac Islam Chachagua 25-0464-2017",
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15)),
              Expanded(child: _lista())
            ],
          )),
    );
  }

  Future<List<dynamic>> cargar() async {
    final response = await http
        .get(Uri.parse("https://utecclass.000webhostapp.com/post.php"));

    String body;
    if (response.statusCode == 200) {
      body = utf8.decode(response.bodyBytes);
      final jsonData = jsonDecode(body);
      return jsonData;
    }
    return [];
  }

  Widget _lista() {
    return FutureBuilder(
      future: cargar(),
      builder: (context, AsyncSnapshot<List<dynamic>> snapshot) {
        return ListView(
          children: _listItems(snapshot.data),
        );
      },
    );
  }

  List<Widget> _listItems(List<dynamic>? data) {
    final List<Widget> opciones = [];
    data?.forEach((item) {
      final widgetTemp = ListTile(
        leading: Icon(Icons.account_circle),
        title:
            Text(item['title'], style: TextStyle(fontWeight: FontWeight.bold)),
        subtitle: Text(item['content']),
        onTap: () {
        },
      );
      opciones.add(widgetTemp);
    });
    return opciones;
  }
}
